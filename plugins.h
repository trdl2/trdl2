#ifndef PLUGINS_H
#define PLUGINS_H

#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <tuple>

#include "utils.h"

class ThreadManager;
class DataHash;

class Plugin {
public:
  virtual ~Plugin()
  {}
  static bool compare(const RValueType &, const RValueType *, ThreadManager *) { return false; }
  static bool compare2(const RValueType &, const DataHash &, ThreadManager *) { return false; }
  static void execute(const std::string &, const RValueType &, DataHash &, ThreadManager *) {}
  static void execute2(const RValueType &, DataHash &, ThreadManager *) {}
  static void execute3(const FieldStatementMap &, DataHash &, ThreadManager *) {}
};

class PluginRegistry {
public:

  struct register_base
  {
    virtual const char* id() const = 0;
    virtual bool compare_(const RValueType &lvalues, const RValueType *rvalues, ThreadManager *h) = 0;
    virtual bool compare2_(const RValueType &lvalues, const DataHash &hash, ThreadManager *h) = 0;
    virtual void execute_(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h) = 0;
    virtual void execute2_(const RValueType &lvalues, DataHash &hash, ThreadManager *h) = 0;
    virtual void execute3_(const FieldStatementMap &lvalues, DataHash &hash, ThreadManager *h) = 0;
  };

  template<class C>
  struct register_ : register_base
  {
    static const char* ID;
    register_() :
      id_(ID)
    {}
    const char* id() const
    {
      return C::factory_key;
    }
    bool compare_(const RValueType &lvalues, const RValueType *rvalues, ThreadManager *h)
    {
      return C::compare(lvalues, rvalues, h);
    }
    bool compare2_(const RValueType &lvalues, const DataHash &hash, ThreadManager *h)
    {
      return C::compare2(lvalues, hash, h);
    }
    void execute_(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
    {
      C::execute(field, lvalues, hash, h);
    }
    void execute2_(const RValueType &lvalues, DataHash &hash, ThreadManager *h)
    {
      C::execute2(lvalues, hash, h);
    }
    void execute3_(const FieldStatementMap &lvalues, DataHash &hash, ThreadManager *h)
    {
      C::execute3(lvalues, hash, h);
    }

  private:
    const char* id_;
  };

  static const char* Register(register_base* message)
  {
    logger(LOG_DEBUG) << "Registering plugin '" << message->id() << "'" << std::endl;
    std::tuple<const char*, register_base*> t(message->id(), message);
    m_list.push_back(t);
    return message->id();
  }

  static int getId(std::string name)
  {
    for (size_t i = 0; i < m_list.size(); i++)
      if (name.compare(std::get<0>(m_list.at(i))) == 0)
        return i;
    logger(LOG_WARNING) << "No such plugin found: " << name << std::endl;
    return -1;
  }

  static std::string getName(unsigned int id)
  {
    if (id >= m_list.size())
    {
      logger(LOG_ERROR) << "Out of bounds id encountered: " << id << std::endl;
      return "Invalid";
    }
    return std::string(std::get<0>(m_list.at(id)));
  }

  static size_t getPluginCount()
  {
    return m_list.size();
  }

  static bool compare(unsigned int id, const RValueType &lvalues, const RValueType *rvalues, ThreadManager *h)
  {
    if (id >= m_list.size())
    {
      logger(LOG_ERROR) << "Out of bounds id encountered: " << id << std::endl;
      return false;
    }
    return std::get<1>(m_list.at(id))->compare_(lvalues, rvalues, h);
  }

  static bool compare2(unsigned int id, const RValueType &lvalues, const  DataHash &hash, ThreadManager *h)
  {
    if (id >= m_list.size())
    {
      logger(LOG_ERROR) << "Out of bounds id encountered: " << id << std::endl;
      return false;
    }
    return std::get<1>(m_list.at(id))->compare2_(lvalues, hash, h);
  }

  static bool execute(unsigned int id, const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    if (id >= m_list.size())
    {
      logger(LOG_ERROR) << "Out of bounds id encountered: " << id << std::endl;
      return false;
    }
    std::get<1>(m_list.at(id))->execute_(field, lvalues, hash, h);
    return true;
  }

  static bool execute2(unsigned int id, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    if (id >= m_list.size())
    {
      logger(LOG_ERROR) << "Out of bounds id encountered: " << id << std::endl;
      return false;
    }
    std::get<1>(m_list.at(id))->execute2_(lvalues, hash, h);
    return true;
  }

  static bool execute3(unsigned int id, const FieldStatementMap &lvalues, DataHash &hash, ThreadManager *h)
  {
    if (id >= m_list.size())
    {
      logger(LOG_ERROR) << "Out of bounds id encountered: " << id << std::endl;
      return false;
    }
    std::get<1>(m_list.at(id))->execute3_(lvalues, hash, h);
    return true;
  }

  static std::vector<std::tuple<const char*, register_base*> > m_list;
};

#endif // PLUGINS_H
