#include "utils.h"

#include "datahash.h"
#include "threadmanager.h"

#include <boost/regex.hpp>

template <class T>
StringList utils::evaluate(const T &oldstrings, const DataHash &hash, const ThreadManager *h)
{
  boost::regex variable_regex(R"var((?<!\\)\$([[:word:]]+|\{.*?(?<!\\)\}))var");


  StringList strings;
  for (const auto &l : oldstrings)
    strings.push_back(l);


  StringList::iterator currentString = strings.begin();

  while(currentString != strings.end())
  {
    boost::match_results<std::string::iterator> what;
    boost::match_flag_type flags = boost::match_default;

    while(regex_search(currentString->begin(), currentString->end(), what, variable_regex, flags))
    {
      RValueType replacement;

      std::string var = what[1];

      if (var[0] == '{' && var[var.size()-1] == '}')
      {
        var.erase(0, 1);
        var.erase(var.size()-1);
      }
      if (is_global(var))
      {
        std::lock_guard<std::recursive_mutex> lock(h->global_lock);
        auto it = h->global.find(var);
        if (it != h->global.end())
          replacement = it->second;
      }
      else
      {
        auto it = hash.find(var);
        if (it != hash.end())
          replacement = it->second;
      }

      for (std::string s : replacement)
      {
        std::string newstring(currentString->c_str());

        std::string::iterator start = newstring.begin(), stop = newstring.begin();
        int fi = std::distance(currentString->begin(), what[0].first);
        int si = std::distance(currentString->begin(), what[0].second);
        std::advance(start, fi);
        std::advance(stop, si);

        newstring.replace(start, stop, s);
        strings.insert(currentString, newstring);
      }
      strings.erase(currentString);
      if (strings.size() == 0)
        return strings;
      std::advance(currentString, -replacement.size());

    }
    currentString++;
  }

  return strings;
}

StringList utils::evaluate(const StringList &oldstrings, const DataHash &hash, const ThreadManager *h)
{
  return evaluate<StringList>(oldstrings, hash, h);
}

StringList utils::evaluate(const StringVector &oldstrings, const DataHash &hash, const ThreadManager *h)
{
  return evaluate<StringVector>(oldstrings, hash, h);
}

StringList utils::evaluate(const std::string &oldstring, const DataHash &hash, const ThreadManager *h)
{
  StringVector v;
  v.push_back(oldstring);
  return evaluate<StringVector>(v, hash, h);
}

Logger& Logger::instance() {
  static Logger l;
  return l;
}
