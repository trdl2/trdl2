#ifndef DEFINES_H
#define DEFINES_H

#include <string>
#include <vector>
#include <list>
#include <map>

#define ACTIONS_STRING  "_actions"

#define CURRENT_ACTION_STRING "_current_action"
#define CURRENT_FILTER_STRING "_current_filter"

#define FILTERS_STRING  "filters"
#define FLAGS_STRING    "options"

// Flags
#define RUN_ALL_STRING  "run_all"

#define IF_STRING   "if"
#define THEN_STRING "then"
#define ELSE_STRING "else"

// Plugins
#define EQUALS_ANY_STRING     "equals_any"
#define EQUALS_ALL_STRING     "equals_all"
#define EQUALS_NOT_STRING     "equals_not"
#define REGEX_MATCH_STRING    "regex_match"
#define REGEX_REPLACE_STRING  "regex_replace"
#define ISSET_ALL_STRING      "isset_all"
#define EXEC_STRING           "exec"
#define TAILCOMMAND_STRING    "followcommand"
#define TAILPIPE_STRING       "followpipe"
#define SET_STRING            "set"
#define UNSET_STRING          "unset"
#define POPFRONT_STRING       "popfront"
#define POPBACK_STRING        "popback"
#define PUSHFRONT_STRING      "pushfront"
#define PUSHBACK_STRING       "pushback"
#define PRINT_STRING          "print"
#define SPLIT_STRING          "split"
#define JOIN_STRING           "join"
#define SPAWN_STRING          "spawn"
#define READLINE_STRING       "prompt"

// tailcommand
#define TC_COMMAND_STRING "command"
#define TC_PIPE_STRING    "pipe"
#define TC_ACTIONS_STRING "actions"
#define TC_STDOUT_STRING  "output_to"
#define TC_FIELDS_STRING  "fileds"



typedef std::list<std::string> StringList;
typedef std::vector<std::string> StringVector;

typedef StringList RValueType;
typedef StringVector LValueType;

typedef LValueType ValueList;

typedef std::string Field;
typedef unsigned int PluginId;

typedef std::map<Field, ValueList > FieldStatementMap;

struct FieldStatements {
  FieldStatementMap map;
  LValueType values;
};

typedef std::map<PluginId, FieldStatements > StatementMap;

struct Filter {
  StatementMap conditions;
  StatementMap action1;
  StatementMap action2;
};

typedef std::vector<Filter> FilterList;

struct Action {
  std::string name;
  FilterList filters;
  uint flags;
};

typedef std::map<std::string, Action> ActionMap;

#endif // DEFINES_H
