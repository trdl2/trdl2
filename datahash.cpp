#include "datahash.h"

DataHash::DataHash()
{
}

bool DataHash::popFront(std::string s, std::string *v)
{
  auto it = find(s);
  if (it == end())
    return false;

  RValueType &l = it->second;
  if (l.empty())
    return false;

  if (v != nullptr)
    *v = l.front();
  l.pop_front();

  if (l.empty())
    erase(s);

  return true;
}

bool DataHash::popBack(std::string s, std::string *v)
{
  auto it = find(s);
  if (it == end())
    return false;

  RValueType &l = it->second;
  if (l.empty())
    return false;

  if (v != nullptr)
    *v = l.back();
  l.pop_back();

  if (l.empty())
    erase(s);

  return true;
}

void DataHash::pushFront(std::string s, std::string v)
{
  RValueType &l = (*this)[s];
  l.push_front(v);
}

void DataHash::pushBack(std::string s, std::string v)
{
  RValueType &l = (*this)[s];
  l.push_back(v);
}
