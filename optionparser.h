#ifndef OPTIONPARSER_H
#define OPTIONPARSER_H

#include <string>
#include <vector>

#include <iostream>
#include <sstream>

class OptionParser
{
public:

  class Option {
  public:
    Option(const char short_opt, const char *long_opt, bool need_arg = false) :
      m_short(short_opt),
      m_long(long_opt),
      m_need_arg(need_arg)
    {}
    virtual ~Option() {}
    virtual void setValue(std::string s) = 0;
    const char m_short;
    const char* m_long;
    bool m_need_arg;
  };

  OptionParser();
  ~OptionParser();

  void add(Option *o);
  bool parse(int &argc, char **&argv);

private:

  std::vector<Option*> m_options;
};


namespace Option {

template <typename T>
class Single : public OptionParser::Option {
public:
  Single(T *ptr, const char short_opt, const char *long_opt = nullptr) :
      Option(short_opt, long_opt, true), m_ptr(ptr) {}
  Single(T *ptr, const char *long_opt, const char short_opt = ' ') :
      Option(short_opt, long_opt, true), m_ptr(ptr) {}
  virtual void setValue(std::string s)
  {
      T obj;
      std::stringstream ss(s);
      ss >> obj;
      *m_ptr = obj;
  }
  T* m_ptr;
};

template <typename T>
class Multiple : public OptionParser::Option {
public:
  Multiple(std::vector<T> *ptr, const char short_opt, const char *long_opt = nullptr) :
    Option(short_opt, long_opt, true), m_ptr(ptr) {}
  Multiple(std::vector<T> *ptr, const char *long_opt, const char short_opt = ' ') :
    Option(short_opt, long_opt, true), m_ptr(ptr) {}
  virtual void setValue(std::string s)
  {
      T obj;
      std::stringstream ss;
      ss << s;
      ss >> obj;
      m_ptr->push_back(obj);
  }
  std::vector<T>* m_ptr;
};

template <typename T>
class Counter : public OptionParser::Option {
public:
  Counter(T *ptr, const char short_opt, const char *long_opt = nullptr) :
    Option(short_opt, long_opt, false), m_ptr(ptr) {}
  Counter(T *ptr, const char *long_opt, const char short_opt = ' ') :
    Option(short_opt, long_opt, false), m_ptr(ptr) {}
  virtual void setValue(std::string) { (*m_ptr)++; }
  T* m_ptr;
};

}

#endif // OPTIONPARSER_H
