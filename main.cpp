
#include <iostream>
#include <fstream>

#include <sys/types.h>
#include <dirent.h>

#include "optionparser.h"
#include "plugins.h"
#include "utils.h"
#include "threadmanager.h"
#include "yaml.h"

ActionMap readConfig(StringVector paths)
{
  StringVector newpaths;

  for (auto p = paths.begin(), end = paths.end(); p != end; p++)
  {
    DIR *dp;
    struct dirent *dirp;

    if((dp  = opendir(p->c_str())) == NULL)
    {
      if (errno == ENOTDIR)
      {
        if (p->substr(p->length()-4).compare(".yml") == 0 || p->substr(p->length()-5).compare(".yaml") == 0)
          newpaths.push_back(*p);
      }
    }
    else
    {
      while ((dirp = readdir(dp)) != NULL)
      {
        std::string entry(*p + "/" +dirp->d_name);

        if (entry.substr(entry.length()-4).compare(".yml") == 0 || entry.substr(entry.length()-5).compare(".yaml") == 0)
          newpaths.push_back(entry);
      }

      closedir(dp);
    }
  }

  ActionMap actions, map;

  logger(LOG_INFO) << "Using configfiles: " << std::endl;
  for (std::string &s : newpaths)
  {
    logger(LOG_INFO) << " " << s << std::endl;
    std::ifstream fin(s);
    YAML::Parser parser(fin);

    YAML::Node doc;
    parser.GetNextDocument(doc);
    doc >> map;
    actions.insert(map.begin(), map.end());
  }
  return actions;
}

int main(int argc, char* argv[])
{
  OptionParser op;

  StringVector configfiles;
  StringVector initactions;

  unsigned int verbosity(3);
  unsigned int quiet(0);
  bool list(false);

  op.add(new Option::Multiple<std::string>(&configfiles, 'c', "conf"));
  op.add(new Option::Multiple<std::string>(&initactions, 'a', "action"));
  op.add(new Option::Counter<unsigned int>(&verbosity, 'v', "verbose"));
  op.add(new Option::Counter<unsigned int>(&quiet, 'q', "quiet"));
  op.add(new Option::Counter<bool>(&list, 'l', "list-plugins"));

  op.parse(argc, argv);
  verbosity = quiet > verbosity ? 0 : verbosity - quiet;

  logger.setLogLevel(verbosity > (unsigned int)LOG_DEBUG ? LOG_DEBUG : (log_level_t)verbosity);

  if (list)
  {
    std::cout << "Registered plugins (" << PluginRegistry::getPluginCount() << "):" << std::endl;
    for (unsigned int i = 0; i < PluginRegistry::getPluginCount(); i++)
      std::cout << " " << PluginRegistry::getName(i) << std::endl;
  }


  ActionMap actions = readConfig(configfiles);

  ThreadManager handler(actions);

  for (auto a : initactions)
  {
    DataHash hash;
    hash.pushBack(ACTIONS_STRING, a);
    handler.push(hash);
  }

  handler.processQueue();

  return 0;
}
