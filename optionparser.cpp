#include "optionparser.h"

#include <iostream>

OptionParser::OptionParser()
{
}

OptionParser::~OptionParser()
{
  for (unsigned int i = 0; i < m_options.size(); i++)
    delete m_options.at(i);
}

bool OptionParser::parse(int &argc, char**& argv)
{
  bool parse(true);
  int i(1);

  while(parse && i < argc) {
    std::string arg(argv[i]);

//    std::cout << "got arg <" << arg << ">" << std::endl;

    if (arg.length() > 2 && arg[0] == '-' && arg[1] == '-')
    {
      std::string param, param_arg;

      int found_index = arg.find_first_of('=', 2);
      if (found_index > 0)
      {
        param_arg = arg.substr(found_index+1);
      }

      param = arg.substr(2, found_index > 0 ? found_index-2 : arg.length());

      for (Option *o : m_options)
      {
        if (o->m_long != nullptr && param.compare(o->m_long) == 0)
        {
          if (o->m_need_arg && found_index < 0)
          {
            i++;
            if (i < argc)
            {
              param_arg = argv[i];
            }
            else
            {
              std::cerr << "NEED ARG BUT WON'T GET ANY!" << std::endl;
              return false;
            }
          }
          o->setValue(param_arg);
          break;
        }
      }
    }
    else if (arg.length() > 1 && arg[0] == '-')
    {
      std::string param_arg;

      for (unsigned int c = 1; c < arg.length(); c++)
      {
        char param = arg[c];

        for (Option *o : m_options)
        {
          if (o->m_short == param)
          {
            if (o->m_need_arg)
            {
              if (c < arg.length() - 1)
              {
                param_arg = arg.substr(c+1);
                if (param_arg[0] == '=') param_arg.erase(0, 1);
                c = arg.length();
              }
              else
              {
                i++;
                if (i < argc)
                  param_arg = argv[i];
                else
                {
                  std::cerr << "NEED ARG BUT WON'T GET ANY!" << std::endl;
                  return false;
                }
              }
            }
            o->setValue(param_arg);
            break;
//            std::cout << "got param <" << param << "> with arg <" << param_arg << ">" << std::endl;
          }
        }
      }
    }

    i++;
  }
//  for (Option *o : m_options)
//  {
//    if (*(o->m_count) < 1 && o->m_mandatory)
//    {
//      std::cerr << "MANDATORY ARGUMENT --" << (o->m_long == nullptr ? "" : o->m_long) << "/-" << o->m_short << " WAS NOT PRESENT!" << std::endl;
//      return false;
//    }
//  }
  return true;
}

void OptionParser::add(Option *o)
{
  if (o->m_long == nullptr && o->m_short == ' ')
    return;

  m_options.push_back(o);
}
