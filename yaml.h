#ifndef YAML_H
#define YAML_H

#include <yaml-cpp/yaml.h>

#include "plugins.h"
#include "defines.h"

static void operator >> (const YAML::Node& node, FieldStatements& fs)
{
  if (node.Type() == YAML::NodeType::Scalar)
  {
    std::string key;
    node >> key;
//    std::cout << "scalar: " << key << std::endl;

    fs.values.push_back(key);
  }
  else if (node.Type() == YAML::NodeType::Sequence)
  {
//    std::cout << "sequance" << std::endl;
    for (size_t i = 0; i < node.size(); i++)
    {
      std::string key;
      node[i] >> key;
      fs.values.push_back(key);
    }
  }
  else if (node.Type() == YAML::NodeType::Map)
  {
//    std::cout << "map" << std::endl;

    for (YAML::Iterator it = node.begin(); it != node.end(); it++)
    {
      std::string key;
      it.first() >> key;

      if (it.second().Type() == YAML::NodeType::Sequence)
      {
        ValueList l;
        it.second() >> l;
        fs.map[key] = l;
      }
      else if (it.second().Type() == YAML::NodeType::Scalar)
      {
        std::string s;
        it.second() >> s;
        ValueList l;
        l.push_back(s);
        fs.map[key] = l;
      }
    }
  }
}

static void operator >> (const YAML::Node& node, StatementMap& sm)
{
  for (YAML::Iterator it = node.begin(); it != node.end(); it++)
  {
    std::string key;
    it.first() >> key;
    int id = PluginRegistry::getId(key);
    if (id < 0)
      std::cerr << "SHOULD SPIT ERROR HERE: " << key << "->" << id << std::endl;
    FieldStatements fs;
    it.second() >> fs;
    if (fs.map.size() > 0 || fs.values.size() > 0)
      sm[id] = fs;
  }
}


static void operator >> (const YAML::Node& node, Filter& filter)
{
  bool anyfound(false);
  if (const YAML::Node *n = node.FindValue(IF_STRING))
  {
    *n >> filter.conditions;
    anyfound = true;
  }

  if (const YAML::Node *n = node.FindValue(THEN_STRING))
  {
    *n >> filter.action1;
    anyfound = true;
  }
  if (const YAML::Node *n = node.FindValue(ELSE_STRING))
  {
    *n >> filter.action2;
    anyfound = true;
  }

  if (!anyfound)
  {
    node >> filter.action1;
  }
}

static void operator >> (const YAML::Node& node, Action& action)
{
  const YAML::Node& filters = node[FILTERS_STRING];

  for (size_t i = 0; i < filters.size(); i++)
  {
    Filter filter;
    filters[i] >> filter;
    action.filters.push_back(filter);
  }

  action.flags = 0;

  if (const YAML::Node *n = node.FindValue(FLAGS_STRING))
  {
    for (size_t i = 0; i < n->size(); i++)
    {
      std::string s;
      (*n)[i] >> s;
      if (s == RUN_ALL_STRING)
        action.flags = action.flags | 1;
    }
  }
}

static void operator >> (const YAML::Node& node, ActionMap& actions)
{
  for (YAML::Iterator it = node.begin(); it != node.end(); it++)
  {
    std::string key;
    it.first() >> key;
    Action action;
    it.second() >> action;
    action.name = key;
    actions[key] = action;
  }
}





static YAML::Emitter& operator << (YAML::Emitter &out, const FieldStatementMap &sm)
{
  out << YAML::BeginMap;
  for (FieldStatementMap::const_iterator it = sm.begin(); it != sm.end(); it++)
  {
    out << YAML::Key << (*it).first;
    out << YAML::Value << (*it).second;
  }
  out << YAML::EndMap;

  return out;
}

static YAML::Emitter& operator << (YAML::Emitter &out, const FieldStatements &fs)
{
  if (fs.map.size() > 0)
    out << fs.map;
  else
    out << fs.values;

  return out;
}

static YAML::Emitter& operator << (YAML::Emitter &out, const StatementMap &sm)
{
  out << YAML::BeginMap;
  for (StatementMap::const_iterator it = sm.begin(); it != sm.end(); it++)
  {
    out << YAML::Key << PluginRegistry::getName((*it).first);
    out << YAML::Value << (*it).second;
  }
  out << YAML::EndMap;

  return out;
}

static YAML::Emitter& operator << (YAML::Emitter &out, const Filter &filter)
{
  out << YAML::BeginMap;
  out << YAML::Key << IF_STRING;
  out << YAML::Value << filter.conditions;
  out << YAML::Key << THEN_STRING;
  out << YAML::Value << filter.action1;
  out << YAML::Key << ELSE_STRING;
  out << YAML::Value << filter.action2;
  out << YAML::EndMap;
  return out;
}

static YAML::Emitter& operator << (YAML::Emitter &out, const Action &action)
{
  out << YAML::BeginMap;
  out << YAML::Key << FILTERS_STRING;
  out << YAML::Value;
  out << YAML::BeginSeq;
  for (FilterList::const_iterator it = action.filters.begin(); it != action.filters.end(); it++)
  {
    out << (*it);
  }
  out << YAML::EndSeq;
  out << YAML::Key << FLAGS_STRING;
  out << YAML::Value;
  out << YAML::BeginSeq;

  if (action.flags & 1)
    out << RUN_ALL_STRING;

  out << YAML::EndSeq;
  out << YAML::EndMap;
  return out;
}

static YAML::Emitter& operator << (YAML::Emitter &out, const ActionMap &actions)
{
  out << YAML::BeginMap;
  for (ActionMap::const_iterator it = actions.begin(); it != actions.end(); it++)
  {
    out << YAML::Key << (*it).first;
    out << YAML::Value << (*it).second;
  }
  out << YAML::EndMap;
  return out;
}

#endif // YAML_H
