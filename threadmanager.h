#ifndef CONCURRENTQUEUE_H
#define CONCURRENTQUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <iostream>

#include "defines.h"
#include "datahash.h"

class ThreadManager
{
public:
  ThreadManager(const ActionMap &actions);
  void push(DataHash const& data);
  void processData(DataHash h);
  void thread_finished();
  bool processQueue();

private:
  std::queue<DataHash> m_queue;
  mutable std::mutex m_mutex;
  std::list<std::thread*> active_threads, inactive_threads;
  std::condition_variable m_cond_var;

public:
  const ActionMap m_actions;
  DataHash global;
  mutable std::recursive_mutex global_lock;

};

#endif // CONCURRENTQUEUE_H
