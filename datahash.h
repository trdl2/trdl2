#ifndef DATAHASH_H
#define DATAHASH_H

#include "defines.h"

typedef std::map<std::string, RValueType> DataHash_;

class DataHash : public DataHash_
{
public:
  DataHash();

  bool popFront(std::string s, std::string *v = nullptr);
  bool popBack(std::string s, std::string *v = nullptr);

  void pushFront(std::string s, std::string v);
  void pushBack(std::string s, std::string v);

//  std::map<std::string, StringList> m_map;
};

#endif // DATAHASH_H
