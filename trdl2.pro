#-------------------------------------------------
#
# Project created by QtCreator 2012-11-26T18:10:26
#
#-------------------------------------------------

QT       -= core

QT       -= gui

TARGET = trdl2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -lboost_regex

SOURCES += main.cpp \
    optionparser.cpp \
    plugins.cpp \
    datahash.cpp \
    utils.cpp \
    threadmanager.cpp


unix {
    CONFIG += link_pkgconfig
    PKGCONFIG += yaml-cpp
    QMAKE_CXXFLAGS += -std=c++11
}

HEADERS += \
    optionparser.h \
    plugins.h \
    defines.h \
    datahash.h \
    utils.h \
    yaml.h \
    threadmanager.h
