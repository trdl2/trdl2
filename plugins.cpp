#include "plugins.h"

#include "threadmanager.h"
#include "yaml.h"

#include <iostream>
#include <sstream>
#include <fstream>

#include <pstreams/pstream.h>

std::vector<std::tuple<const char*, PluginRegistry::register_base*> > PluginRegistry::m_list;

template<class C>
const char* PluginRegistry::register_<C>::ID = PluginRegistry::Register(new PluginRegistry::register_<C>());


//class RegexPlugin: public Plugin, private PluginRegistry::register_<RegexPlugin> {
//public:
//  static constexpr const char* factory_key = REGEX_STRING;
//  RegexPlugin() {}

//};

class IssetAllPlugin : public Plugin, private PluginRegistry::register_<IssetAllPlugin> {
public:
  static constexpr const char* factory_key = ISSET_ALL_STRING;
  IssetAllPlugin() {}

  static bool compare2(const RValueType &lvalues, const DataHash &hash, ThreadManager *h)
  {
    for (const auto &l : lvalues)
    {
      if (utils::is_global(l))
      {
        std::lock_guard<std::recursive_mutex> lock(h->global_lock);
        if (h->global.find(l) == h->global.end())
          return false;
      }
      else
        if (hash.find(l) == hash.end())
          return false;
    }
    return true;
  }
};

class EqualsAnyPlugin : public Plugin, private PluginRegistry::register_<EqualsAnyPlugin> {
public:
  static constexpr const char* factory_key = EQUALS_ANY_STRING;
  EqualsAnyPlugin() {}

  static bool compare(const RValueType &lvalues, const RValueType *rvalues, ThreadManager */*h*/)
  {
//    std::cout << "Running plugin " << factory_key << std::endl;
    if (rvalues == nullptr) return false;

    for (auto &r : *rvalues)
      for (const auto &l : lvalues)
        if (r == l)
          return true;
    return false;
  }
};

class EqualsNotPlugin : public Plugin, private PluginRegistry::register_<EqualsNotPlugin> {
public:
  static constexpr const char* factory_key = EQUALS_NOT_STRING;
  EqualsNotPlugin() {}

  static bool compare(const RValueType &lvalues, const RValueType *rvalues, ThreadManager *h)
  {
    return !EqualsAnyPlugin::compare(lvalues, rvalues, h);
  }
};

class EqualsAllPlugin : public Plugin, private PluginRegistry::register_<EqualsAllPlugin> {
public:
  static constexpr const char* factory_key = EQUALS_ALL_STRING;
  EqualsAllPlugin() {}

  static bool compare(const RValueType &lvalues, const RValueType *rvalues, ThreadManager */*h*/)
  {
    if (rvalues == nullptr) return false;

    for (const auto &l : lvalues)
    {
      bool exist(false);
      for (auto &r : *rvalues)
        if (r == l)
        {
          exist = true;
          break;
        }
      if (!exist)
        return false;
    }
    return true;
  }
};

class PrintPlugin : public Plugin, private PluginRegistry::register_<PrintPlugin> {
public:
  static constexpr const char* factory_key = PRINT_STRING;
  PrintPlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    if (field == "special")
    {
      for (const auto &l : lvalues)
      {
        if (l == "data")
        {
          std::cout << "-----data-----" << std::endl;
          for (auto i : hash)
          {
            if (i.first.size() > 0 && i.first[0] == '_')
              continue;
            std::cout << i.first << ":" << (i.second.size() == 0 ? " {}" : "") << std::endl;
            for (auto j : i.second)
              std::cout << "- " << j << std::endl;
          }
          std::cout << "--------------" << std::endl;
        }
        else if (l == "globals")
        {
          std::cout << "----globals---" << std::endl;
          std::lock_guard<std::recursive_mutex> lock(h->global_lock);
          for (auto i : h->global)
          {
            std::cout << i.first << ":" << (i.second.size() == 0 ? " {}" : "") << std::endl;
            for (auto j : i.second)
              std::cout << "- " << j << std::endl;
          }
          std::cout << "--------------" << std::endl;
        }
        else if (l == "actions")
        {
          std::cout << "----actions---" << std::endl;
          YAML::Emitter out;
          out << h->m_actions;
          std::cout << out.c_str() << std::endl;
          std::cout << "--------------" << std::endl;
        }
      }
    }
    else if (field == "string")
    {
      for (const auto &l : lvalues)
      {
        std::cout << l << std::endl;
      }
    }
  }
};

class ExecPlugin:public Plugin, private PluginRegistry::register_<ExecPlugin> {
public:
  static constexpr const char* factory_key = EXEC_STRING;
  ExecPlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    for (const auto &l : lvalues)
    {
      redi::ipstream ps;
      ps.open(l);
      std::stringstream out;
      std::string line;

      auto &i = ps;
      while (std::getline(i, line))
      {
        out << line << std::endl;
      }
      ps.close();
      line = out.str();

      if (utils::is_global(field))
      {
        std::lock_guard<std::recursive_mutex> lock(h->global_lock);
        h->global.pushBack(field, utils::trim(line));
      }
      else
        hash.pushBack(field, utils::trim(line));
    }
  }
};

class TailCommandPlugin:public Plugin, private PluginRegistry::register_<TailCommandPlugin> {
public:
  static constexpr const char* factory_key = TAILCOMMAND_STRING;
  TailCommandPlugin() {}

  static void execute3(const FieldStatementMap &lvalues, DataHash &hash, ThreadManager *h)
  {
    auto it = lvalues.find(TC_COMMAND_STRING);
    if (it == lvalues.end())
      return;
    ValueList commands = it->second;

    it = lvalues.find(TC_ACTIONS_STRING);
    ValueList actions;
    if (it != lvalues.end())
      actions = it->second;

    it = lvalues.find(TC_STDOUT_STRING);
    ValueList strings;
    if (it != lvalues.end())
      strings = it->second;

    it = lvalues.find(TC_FIELDS_STRING);
    ValueList fields;
    if (it != lvalues.end())
      fields = it->second;


    for (const auto &c : commands)
    {
      redi::ipstream ps;

      ps.open(c);

      std::string line;

      auto &i = ps;
      while (std::getline(i, line))
      {
        DataHash newhash;

        line = utils::trim(line);

        for (const auto &f : fields)
        {
          auto it = hash.find(f);
          if (it == hash.end())
            continue;
          for (auto i : it->second)
            newhash.pushBack(f, i);
        }

        for (const auto &a : actions)
          newhash.pushBack(ACTIONS_STRING, a);

        for (const auto &s : strings)
          newhash.pushBack(s, line);

        h->push(newhash);
      }
    }
  }
};

class TailPipePlugin:public Plugin, private PluginRegistry::register_<TailPipePlugin> {
public:
  static constexpr const char* factory_key = TAILPIPE_STRING;
  TailPipePlugin() {}

  static void execute3(const FieldStatementMap &lvalues, DataHash &hash, ThreadManager *h)
  {
    auto it = lvalues.find(TC_PIPE_STRING);
    if (it == lvalues.end())
      return;
    ValueList pipes = it->second;

    it = lvalues.find(TC_ACTIONS_STRING);
    ValueList actions;
    if (it != lvalues.end())
      actions = it->second;

    it = lvalues.find(TC_STDOUT_STRING);
    ValueList strings;
    if (it != lvalues.end())
      strings = it->second;

    it = lvalues.find(TC_FIELDS_STRING);
    ValueList fields;
    if (it != lvalues.end())
      fields = it->second;


    for (const auto &p : pipes)
    {
      std::ifstream ps;

      ps.open(p);

      std::string line;

      auto &i = ps;
      while (std::getline(i, line))
      {
        DataHash newhash;

        line = utils::trim(line);

        for (const auto &f : fields)
        {
          auto it = hash.find(f);
          if (it == hash.end())
            continue;
          for (auto i : it->second)
            newhash.pushBack(f, i);
        }

        for (const auto &a : actions)
          newhash.pushBack(ACTIONS_STRING, a);

        for (const auto &s : strings)
          newhash.pushBack(s, line);

        h->push(newhash);
      }
    }
  }
};

class ReadLinePlugin:public Plugin, private PluginRegistry::register_<ReadLinePlugin> {
public:
  static constexpr const char* factory_key = READLINE_STRING;
  ReadLinePlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    for (const auto &l : lvalues)
    {
      std::cout << l << std::flush;
    }
    std::string line;
    std::getline(std::cin, line);

    if (utils::is_global(field))
    {
      std::lock_guard<std::recursive_mutex> lock(h->global_lock);
      h->global.pushBack(field, utils::trim(line));
    }
    else
      hash.pushBack(field, utils::trim(line));
  }
};

//class TailPipePlugin:public Plugin, private PluginRegistry::register_<TailPipePlugin> {
//public:
//  static constexpr const char* factory_key = TAILPIPE_STRING;
//  TailPipePlugin() {}

//  static bool compare(const LValueType &lvalues, const RValueType &rvalues, Handler *h) {
//    std::cout << "Running compareplugin " << factory_key << std::endl;
//    return true;
//  }
//  static bool execute(const LValueType &lvalues, RValueType *rvalues, Handler *h) {
//    std::cout << "Running executeplugin " << factory_key << std::endl;
//    return true;
//  }
//};

class SetPlugin : public Plugin, private PluginRegistry::register_<SetPlugin> {
public:
  static constexpr const char* factory_key = SET_STRING;
  SetPlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    std::unique_lock<std::recursive_mutex> lock(h->global_lock, std::defer_lock_t());
    DataHash &global_or_local = utils::is_global(field) ? h->global : hash;

    if (utils::is_global(field)) lock.lock();

    global_or_local.erase(field);

    for (auto &l : lvalues)
    {
      global_or_local.pushBack(field, l);
    }
    if (utils::is_global(field)) lock.unlock();
  }
};

class UnsetPlugin : public Plugin, private PluginRegistry::register_<UnsetPlugin> {
public:
  static constexpr const char* factory_key = UNSET_STRING;
  UnsetPlugin() {}

  static void execute2(const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    for (const auto &l : lvalues)
    {
      if (utils::is_global(l))
      {
        std::lock_guard<std::recursive_mutex> lock(h->global_lock);
        h->global.erase(l);
      }
      else
        hash.erase(l);
    }
  }
};

class PopFrontPlugin : public Plugin, private PluginRegistry::register_<PopFrontPlugin> {
public:
  static constexpr const char* factory_key = POPFRONT_STRING;
  PopFrontPlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager */*h*/)
  {
    for (const auto &l : lvalues)
    {
      std::string s;
      hash.popFront(l, &s);
      hash.pushBack(field, s);
    }
  }
};

class PopBackPlugin : public Plugin, private PluginRegistry::register_<PopBackPlugin> {
public:
  static constexpr const char* factory_key = POPBACK_STRING;
  PopBackPlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager */*h*/)
  {
    for (const auto &l : lvalues)
    {
      std::string s;
      hash.popBack(l, &s);
      hash.pushBack(field, s);
    }
  }
};

class PushFrontPlugin:public Plugin, private PluginRegistry::register_<PushFrontPlugin> {
public:
  static constexpr const char* factory_key = PUSHFRONT_STRING;
  PushFrontPlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    std::unique_lock<std::recursive_mutex> lock(h->global_lock, std::defer_lock_t());
    DataHash &global_or_local = utils::is_global(field) ? h->global : hash;

    if (utils::is_global(field)) lock.lock();

    for (auto &l : lvalues)
    {
      global_or_local.pushFront(field, l);
    }
    if (utils::is_global(field)) lock.unlock();
  }
};

class PushBackPlugin:public Plugin, private PluginRegistry::register_<PushBackPlugin> {
public:
  static constexpr const char* factory_key = PUSHBACK_STRING;
  PushBackPlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    std::unique_lock<std::recursive_mutex> lock(h->global_lock, std::defer_lock_t());
    DataHash &global_or_local = utils::is_global(field) ? h->global : hash;

    if (utils::is_global(field)) lock.lock();

    for (auto &l : lvalues)
    {
      global_or_local.pushBack(field, l);
    }
    if (utils::is_global(field)) lock.unlock();
  }
};

class JoinPlugin:public Plugin, private PluginRegistry::register_<JoinPlugin> {
public:
  static constexpr const char* factory_key = JOIN_STRING;
  JoinPlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    std::unique_lock<std::recursive_mutex> lock(h->global_lock, std::defer_lock_t());
    DataHash &global_or_local = utils::is_global(field) ? h->global : hash;


    std::stringstream ss;
    for (auto &l : lvalues)
    {
      ss << l << std::endl;
    }

    std::string s = ss.str();

    s = utils::trim(s);

    if (utils::is_global(field)) lock.lock();
    global_or_local.pushBack(field, s);
    if (utils::is_global(field)) lock.unlock();
  }
};

class SplitPlugin:public Plugin, private PluginRegistry::register_<SplitPlugin> {
public:
  static constexpr const char* factory_key = SPLIT_STRING;
  SplitPlugin() {}

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    std::unique_lock<std::recursive_mutex> lock(h->global_lock, std::defer_lock_t());
    DataHash &global_or_local = utils::is_global(field) ? h->global : hash;

    if (utils::is_global(field)) lock.lock();

    for (auto &l : lvalues)
    {
      StringVector v = utils::split(l, '\n');

      for (auto &e : v)
        global_or_local.pushBack(field, e);
    }
    if (utils::is_global(field)) lock.unlock();
  }
};

class SpawnPlugin : public Plugin, private PluginRegistry::register_<SpawnPlugin> {
public:
  static constexpr const char* factory_key = SPAWN_STRING;
  SpawnPlugin() {}

  static void execute2(const RValueType &lvalues, DataHash &/*hash*/, ThreadManager *h)
  {
    for (const auto &l : lvalues)
    {
      DataHash ha;
      ha.pushBack(ACTIONS_STRING, l);
      h->push(ha);
    }
  }

  static void execute(const std::string &field, const RValueType &lvalues, DataHash &hash, ThreadManager *h)
  {
    DataHash ha;
    ha.pushBack(ACTIONS_STRING, field);

    for (const auto &l : lvalues)
    {
      auto it = hash.find(l);
      if (it != hash.end())
        ha[l] = it->second;
    }
    h->push(ha);
  }
};

