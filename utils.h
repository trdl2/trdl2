#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <sstream>

#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <iostream>

#include "defines.h"

class DataHash;
class ThreadManager;

namespace utils
{
  template <class T>
  static inline std::string to_string (const T& t)
  {
    std::stringstream ss;
    ss << t;
    return ss.str();
  }

  template <class T>
  static inline T from_string (const std::string& s)
  {
    std::stringstream ss(s);
    T t;
    //  if (!(ss >> t))
    ss >> t;
    return t;
  }

  // trim from start
  static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
  }

  // trim from end
  static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
  }

  // trim from both ends
  static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
  }

  static inline bool is_global(const std::string &s) {
    return isupper(s[0]) != 0;
  }

  static inline std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
      std::stringstream ss(s);
      std::string item;
      while(std::getline(ss, item, delim)) {
          elems.push_back(item);
      }
      return elems;
  }

  static inline std::vector<std::string> split(const std::string &s, char delim) {
      std::vector<std::string> elems;
      elems = split(s, delim, elems);
      return elems;
  }

  template <class T>
  StringList evaluate(const T &oldstrings, const DataHash &hash, const ThreadManager *h);
  StringList evaluate(const StringVector &oldstrings, const DataHash &hash, const ThreadManager *h);
  StringList evaluate(const StringList &oldstrings, const DataHash &hash, const ThreadManager *h);
  StringList evaluate(const std::string &oldstring, const DataHash &hash, const ThreadManager *h);
}

enum log_level_t {
    LOG_NOTHING,
    LOG_CRITICAL,
    LOG_ERROR,
    LOG_WARNING,
    LOG_INFO,
    LOG_DEBUG
};

class Logger {
private:
  Logger() : m_level(LOG_WARNING), cur_level(LOG_NOTHING), out(std::cerr) { }
  log_level_t m_level;
  log_level_t cur_level;
  std::ostream &out;

public:
  static Logger& instance();

  void setLogLevel(log_level_t level)
  {
    m_level = level;
  }

  Logger& operator () (log_level_t level)
  {
    cur_level = level;
    return *this;
  }
  Logger& operator << (std::ostream&(*f)(std::ostream&))
  {
    if (m_level >= cur_level)
      out << f;
    return *this;
  }
  template <class T>
  Logger& operator << (T text)
  {
    if (m_level >= cur_level)
      out << text;
    return *this;
  }

};

#define logger Logger::instance()

#endif // UTILS_H
