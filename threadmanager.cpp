#include "threadmanager.h"

#include "plugins.h"
#include "utils.h"

ThreadManager::ThreadManager(const ActionMap &actions) :
  m_actions(actions)
{}

void ThreadManager::push(DataHash const& data)
{
  std::unique_lock<std::mutex> lock(m_mutex);
  m_queue.push(data);
  lock.unlock();
  m_cond_var.notify_one();
}

void ThreadManager::processData(DataHash hash)
{
  std::string action;
  while(hash.popFront(ACTIONS_STRING, &action))
  {
    logger(LOG_DEBUG) << "Thread " << std::this_thread::get_id() << " running action: '" << action << "'" << std::endl;

    auto it = m_actions.find(action);
    if (it == m_actions.end())
    {
      std::cerr << "action not found: '" << action << "'" << std::endl;
      return;
    }

    const Action &a = it->second;

    hash.pushBack(CURRENT_ACTION_STRING, action);

    int current_filter(0);

    for (const Filter &f : a.filters)
    {

      hash.pushBack(CURRENT_FILTER_STRING, utils::to_string(current_filter));

      bool passed(true);

      for (const auto &c : f.conditions)
      {

        const FieldStatements &fs = c.second;
        for (const auto &p : fs.map)
        {
          const LValueType lvalues = p.second;
          StringList fields = utils::evaluate(p.first, hash, this);
          for (auto s : fields)
          {
            const RValueType *rvalues = nullptr;
            if (utils::is_global(s))
            {
              std::lock_guard<std::recursive_mutex> lock(global_lock);
              auto it = global.find(s);
              if (it != global.end())
                rvalues = &(it->second);
            }
            else
            {
              auto it = hash.find(s);
              if (it != hash.end())
                rvalues = &(it->second);
            }

            StringList lv = utils::evaluate(lvalues, hash, this);
            passed = passed && PluginRegistry::compare(c.first, lv, rvalues, this);
          }
        }
        if (fs.values.size() > 0)
        {
          StringList fields = utils::evaluate(fs.values, hash, this);
          passed = passed && PluginRegistry::compare2(c.first, fields, hash, this);
        }
      }
//      std::cout << "filter passed ? " << (passed ? "yes" : "no") << std::endl;

      const StatementMap &statements = passed ? f.action1 : f.action2;

      for (const auto &c : statements)
      {
        const FieldStatements &fs = c.second;
        FieldStatementMap evaluated;
        for (const auto &p : fs.map)
        {
          const LValueType lvalues = p.second;

          StringList fields = utils::evaluate(p.first, hash, this);
          StringList lv = utils::evaluate(lvalues, hash, this);
          StringVector lve;
          for (auto l : lv)
            lve.push_back(l);

          for (auto s : fields)
          {
            evaluated[s] = lve;
            PluginRegistry::execute(c.first, s, lv, hash, this);
          }
        }
        if (evaluated.size() > 0)
          PluginRegistry::execute3(c.first, evaluated, hash, this);
        if (fs.values.size() > 0)
        {
          StringList fields = utils::evaluate(fs.values, hash, this);
          PluginRegistry::execute2(c.first, fields, hash, this);
        }
      }
      if (!(a.flags & 1) && passed)
        break;
      current_filter++;
      hash.popBack(CURRENT_FILTER_STRING);
    }
    hash.popBack(CURRENT_ACTION_STRING);
  }
}

void ThreadManager::thread_finished()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  for (auto i = active_threads.begin(); i != active_threads.end(); i++)
  {
    if ((*i)->get_id() == std::this_thread::get_id())
    {
      inactive_threads.push_back(*i);
      active_threads.erase(i);
      break;
    }
  }
  m_cond_var.notify_one();
}


bool ThreadManager::processQueue()
{
  std::unique_lock<std::mutex> lock(m_mutex);

  while(true)
  {
    while (m_queue.empty())
    {
      for (auto i = inactive_threads.begin(); i != inactive_threads.end(); i++)
      {
        logger(LOG_DEBUG) << "Terminating thread " << (*i)->get_id() << std::endl;
        (*i)->join();
        i = inactive_threads.erase(i);
      }
      if (active_threads.empty())
      {
        lock.unlock();
        return false;
      }
      m_cond_var.wait(lock);
    }

    DataHash value = m_queue.front();

    m_queue.pop();

    active_threads.push_back(new std::thread([this, value](){ processData(value); thread_finished(); }));
}
}
